const Connections = require("./Connection");
let Connection = Connections.Connection;
const CreateTables = require("./CreateTable");
let CreateTable = CreateTables.CreateTable;
const InsertDatas = require("./InsertData");
let InsertData = InsertDatas.InsertData;
const fs = require("fs");

const nameFile = "./cars_data.txt";
const encoding = "utf8";
const cars = fs.readFileSync(nameFile, encoding);
const carsArr = cars.split("\n");

const auto = {
    name: "Auto",
    id_auta: "int PRIMARY KEY",
    id_modelu: "int",
    id_lakieru: "int",
    rok_prod: "text",
    nr_rej: "text",
    id_silnika: "int",
    przebieg: "int",
    cena: "real",
    naped: "text",
};

const Ubezpieczenie = {
    name: "Ubezpieczenie",
    id_ubez: "int PRIMARY KEY",
    id_auta: "int",
    data_zak: "text",
    data_rozp: "text",
    rodzaj: "text",
    skladka: "text",
    foreign: "key (id_auta) references auto (id_auta)",
};

const auta = {
    name: "Auta",
    id: "int PRIMARY KEY",
    marka: "text",
    model: "text",
    kodLakieru: "text",
    rokProdukcji: "text",
    nrRejestracyjny: "text",
    silnik: "text",
    przebieg: "int",
    cena: "int",
    skrzynia: "text",
    typ: "text",
    ileOsub: "int",
    created_at: "text",
    updated_at: "text",
};

const insertAuto = {
    table: "Auto",
    ilosc: 2000,
    id_auta: ["autoIncrement"],
    id_modelu: ["autoIncrement"],
    id_lakieru: ["autoIncrement"],
    rok_prod: ["1920", "1925", "1930", "1935", "1940"],
    nr_rej: ["iBk3MpTx", "zGWeDiow", "FaWa90UV"],
    id_silnika: ["autoIncrement"],
    przebieg: [620543, 232654, 456765],
    cena: [10.6, 12.6, 14.6, 15.6],
    naped: ["FWD", "RWD", "AWD"],
};

const insertUbezpieczenie = {
    table: "Ubezpieczenie",
    ilosc: 2000,
    id_ubez: ["autoIncrement"],
    id_auta: ["autoIncrement"],
    data_zak: ["1960-05-12", "1961-03-06", "1962-03-06"],
    data_rozp: ["1950-05-12", "1951-03-06", "1952-03-06"],
    rodzaj: ["A", "B", "C"],
    skladka: ["2000", "3000", "4000"],
};

// const table = new CreateTable();
// // table.createTable(auto);
// // table.createTable(Ubezpieczenie);
// table.createTable(auta);
// table.closeDb();

// const insert = new InsertData();
// insert.insertData(insertAuto);
// insert.closeDb();

// const insert = new InsertData();
// insert.insertData(insertUbezpieczenie);
// insert.closeDb();

// const insert = new InsertData();
// insert.addFromFile(carsArr, 14, "auta");
// insert.closeDb();

// const select =
// "SELECT * FROM Auto, Ubezpieczenie WHERE Auto.ID_AUTA=Ubezpieczenie.ID_AUTA LIMIT 100000";
// const select =
// "SELECT * FROM Auto, Ubezpieczenie WHERE Auto.ID_AUTA=Ubezpieczenie.ID_AUTA AND ROK_PROD=1940 LIMIT 600000";
// const update = `UPDATE Ubezpieczenie SET SKLADKA = 8000 WHERE ID_AUTA = 7`;
const update = `UPDATE Ubezpieczenie SET SKLADKA = 4500 WHERE RODZAJ='C'`;
const deleteSql = `DELETE FROM Auta`;
const select = "SELECT * FROM Auta";

const connection = new Connection();
connection.selectData(select);
// connection.updateData(update);
// connection.deleteData(deleteSql);
// connection.dropTable("Ubezpieczenie");
