const Connections = require("./Connection");
let Connection = Connections.Connection;

class CreateTable extends Connection {
    constructor() {
        super();
    }

    createTable(data) {
        if (!data.hasOwnProperty("name")) {
            return console.log("Nie podano parametru name. Spróbuj ponownie.");
        }

        let value = "";
        const { name, ...newData } = data;
        const len = Object.keys(newData).length;

        Object.entries(newData).forEach((val, i) => {
            val.forEach((item, j) => {
                item = item.toUpperCase();
                if (len - 1 === i) {
                    if (j + 1 === val.length) {
                        value += `${item} `;
                    } else {
                        value += `${item} `;
                    }
                } else {
                    if (j + 1 === val.length) {
                        value += `${item}, `;
                    } else {
                        value += `${item} `;
                    }
                }
            });
        });

        this.db.serialize(() => {
            this.db.run(`DROP TABLE IF EXISTS ${name.toUpperCase()}`);
            this.db.run(`CREATE TABLE ${name.toUpperCase()}(${value})`);
        });
    }
}

module.exports = { CreateTable: CreateTable };
