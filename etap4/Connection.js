class Connection {
    constructor() {
        this.sqlite3 = require("sqlite3").verbose();
        this.db = this.startDb();
    }

    startDb() {
        return new this.sqlite3.Database("./komis.db", err => {
            if (err) {
                return console.error(err.message);
            }
            console.log("Connected to the database.");
            console.time("Time");
        });
    }

    closeDb() {
        this.db.close(err => {
            if (err) {
                return console.error(err.message);
            }
            console.timeEnd("Time");
            console.log("Close the database connection.");
        });
    }

    selectData(sql) {
        this.db.serialize(() => {
            this.db.each(sql, [], (err, rows) => {
                if (err) {
                    throw err;
                }
                let value = "";
                for (let row in rows) {
                    value += `${rows[row]} `;
                }
                // console.log(value);
            });
            this.closeDb();
        });
    }

    dropTable(table) {
        this.db.serialize(() => {
            this.db.run(`DROP TABLE IF EXISTS ${table}`);
            this.closeDb();
        });
    }

    updateData(sql) {
        this.db.serialize(() => {
            this.db.run(sql, err => {
                if (err) {
                    return console.log(err.message);
                }
                console.log(`Update danych.`);
            });
            this.closeDb();
        });
    }

    deleteData(sql) {
        this.db.serialize(() => {
            this.db.run(sql, err => {
                if (err) {
                    return console.log(err.message);
                }
                console.log(`Wiersze usunięto.`);
            });
            this.closeDb();
        });
    }
}

module.exports = { Connection: Connection };
